# Automated tests for Petstore swagger API using Serenity BDD framework and Cucumber

This tutorial shows you how to get started with below project - automation for petstore swagger API using Serenity BDD and Cucumber

## Get the code

Git:

    git clone  https://gitlab.com/r_vivek/Petstore.git
    cd Petstore

Or simply [download a zip](https://gitlab.com/r_vivek/Petstore/-/archive/master/Petstore-master.zip) file.

## The swagger petstore API
https://petstore.swagger.io/
![](src/docs/petstore-services.png)

The petstore swagger offers online public API services which can be used to perform automated Rest API testing.
The available end points and their interactions are listed in the screenshot above

##Getting started
The initial step to start with this project is to clone or download the project on Gitlab (https://gitlab.com/r_vivek/Petstore.git). 
This project is about the automation of petstore swagger endpoints using Serenity BDD, Cucumber and SerenityRest-based tests. 
The project automates the testing of all the available end points in the pet store swagger API

### The project directory structure
The project has build scripts only for maven, and follows the standard directory structure used in most Serenity projects:

![](src/docs/project-structure.png)


## A simple GET scenario

The first scenario exercises the `https://petstore.swagger.io/v2/pet` endpoint.
In this scenario, user tries to get a pet from the pet end point and verifies whether the API returns 200 response and also validate
whether the response body matches the json file available in the testfiles folder under resources

```Gherkin
Feature: Pet features in the pet store

  Scenario: Get the updated pet from the petstore app and verify the response
    Given The pet is updated successfully
    When I get the pet with the details from the testdata : "buffy_updated.json"
    Then the API should return a 200 response
    And pet in the response body should have same details as "buffy_updated.json"
```

The glue code for this scenario illustrates the layered approach we find works well for both web and non-web acceptance tests.
The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.

```java
    @Steps
    CommonSteps commonSteps;

    @Steps
    PetSteps petSteps;

    @Given("The pet is updated successfully")
    public void thePetIsUpdated() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @When("I get the pet with the details from the testdata : {string}")
    public void iGetThePetWithTheDetailsFromTheTestdata(String fileName) {
        petSteps.getPet(fileName);
    }
    
    @Then("the API should return a {int} response")
    public void theAPIShouldReturnAResponse(int response) {
        commonSteps.checkResponse(response);
    }
    
    @And("pet in the response body should have same details as {string}")
    public void response_body_should_have_same_details_as(String fileName) {
        petSteps.validateResponse(fileName);
    }
```

The actual REST calls are performed using SerenityRest in the action classes, like `PetSteps`/`CommonSteps` here. 
These use either RestAssured (if we don't want the queries to appear in the reports) or SerenityRest (if we do):

```java
public class PetSteps {

    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String PETENDPOINT = WebServiceEndPoints.PETENDPOINT.getUrl();


    @Step("Get the test data mentioned in {0} from the pet end point")
    public void getPet(String fileName) {
        Pet pet = (Pet) TestUtils.toObject(fileName, Pet.class);
        SerenityRest.given()
                .pathParam("id", pet.getId())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .get(BASEPATH + PETENDPOINT + "/{id}");
    }
}

public class CommonSteps {

    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String ENDPOINTCHECK = WebServiceEndPoints.ENDPOINTCHECK.getUrl();

    @Step("Check whether the response is {0}")
    public void checkResponse(int response) {
        SerenityRest.then()
                .spec(ReuseableSpecifications.getGenericResponseSpec())
                .statusCode(response);
    }

}
```

All the generic steps which can be re-used in all the features are kept under `CommonSteps` whereas the more specific steps
like the POST/PUT call haven been kept under specific Step classes. It can also be noted that a generic Response specification
is used. This will be suited when the services sometimes required specific headers/cookies etc. ``

## A more complex scenario

The other sample scenario performs a POST query:

```gherkin
Feature: User features in the pet store

  Scenario: Add a list of users to the pets store using the createWithList end point
    Given The endpoint is accessible
    When I call the createWithList end point with the below list of users :
      | id  | username     | firstName     | lastName     | email         | password     | phone | userStatus |
      | 111 | username_111 | firstname_111 | lastname_111 | 111@gmail.com | 111_password | 111   | 0          |
      | 222 | username_222 | firstname_222 | lastname_222 | 222@gmail.com | 222_password | 222   | 0          |
      | 333 | username_333 | firstname_333 | lastname_333 | 333@gmail.com | 333_password | 333   | 0          |
    Then the API should return a 200 response
```

The _When_ step uses a dataTable conversion to a list of Map and then uses `com.fasterxml.jackson.databind` available as a
part of the TestUtils class to convert the objects in the map to the specified class

```java
    @When("I call the createWithList end point with the below list of users :")
    public void iCallTheCreateWithListEndPointWithTheBelowListOfUsers(DataTable dataTable) {
        List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        List<User> users = new ArrayList<>();
        rows.forEach(row ->
                users.add((User) TestUtils.toObject(row, User.class)));
        userSteps.addMultipleUsers(users);
    }
    
public class TestUtils {
    
    public static Object toObject(final Map map, final Class aClass) {
        ObjectMapper mapper = new ObjectMapper();
        final Object o = mapper.convertValue(map, aClass);
        return o;
    }
}
```

The `UserSteps` class is responsible for posting this query to the end point, as shown below:

```java
public class UserSteps {

    @Step("Add the list of users in the list {0}")
    public void addMultipleUsers(List<User> users) {
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(users)
                .post(BASEPATH + CREATEUSERLIST);
    }
    
}
```

The last step checks whether the response after the POST is a success 

```java
    @Step("Check whether the response is {0}")
    public void checkResponse(int response) {
        SerenityRest.then()
                .spec(ReuseableSpecifications.getGenericResponseSpec())
                .statusCode(response);
    }
```

## Living documentation

You can generate full Serenity reports by running `mvn clean verify`. 
This includes both the living documentation from the feature files:

![](src/docs/rest-feature.png)

And also details of the REST requests and responses that were executed during the test:

![](src/docs/rest-report.png)


## Installation and execution of tests
1. Download the project source (https://gitlab.com/r_vivek/Petstore/-/archive/master/Petstore-master.zip) file.
Or clone the project using git clone `https://gitlab.com/r_vivek/Petstore.git`
2. Extract the downloaded zip file and execute the test using `-mvn clean verify`

## Writing new tests
1. Download the project source (https://gitlab.com/r_vivek/Petstore/-/archive/master/Petstore-master.zip) file.
Or clone the project using git clone (https://gitlab.com/r_vivek/Petstore.git)
2. Create a feature file with the respective scenarios  under `\src\test\resources\features\petstore` path 
![](src/docs/feature-file.png)
3. Create the respective Step definition files for the feature file created in step 2 under `\src\test\java\starter\stepdefinitions`
![](src/docs/step-definition.png)
4. Create the respective Step file (if needed in case a step definition has multiple steps) under `\src\test\java\starter\steps`
![](src/docs/steps.png)
5. Write the code required to perform the service calls and validations 
6. Execute the tests using `-mvn clean verify`
7. After the build is complete, the reports can be seen in the path `\target\site\serenity\index.html`

## Pipeline configuration
- A CI pipeline has been configured through the yaml file (https://gitlab.com/r_vivek/Petstore/-/blob/master/.gitlab-ci.yml)
such that everytime the source is pushed, it triggers the pipeline which has 2 stages (build & test). The build cleans and compiles
the project with `-mvn clean compile` while test does a `-mvn clean install`
- The test job also generates the reports and attaches them as artifact to the job. Sample report of an executed job (https://r_vivek.gitlab.io/-/Petstore/-/jobs/926037622/artifacts/target/site/serenity/index.html)
- The link to the executed pipeline jobs so far(https://gitlab.com/r_vivek/Petstore/-/pipelines)
- The CI pipeline can also be triggered remotely via a HTTP POST call on the below API end point
(https://gitlab.com/api/v4/projects/23205930/ref/master/trigger/pipeline?token=50a02fb61ad874c6a0024ef7149cd5)
- After triggering the pipeline remotely using the above POST call, you will receive a response like below
```json
{
    "id": 233052784,
    "sha": "c94b95ca67092ddcbbe74f97c76b8203eee4cf01",
    "ref": "master",
    "status": "pending",
    "created_at": "2020-12-21T23:27:12.027Z",
    "updated_at": "2020-12-21T23:27:12.149Z",
    "web_url": "https://gitlab.com/r_vivek/Petstore/-/pipelines/233052784",
    "before_sha": "0000000000000000000000000000000000000000",
    "tag": false,
    "yaml_errors": null,
    "user": {
        "id": 7859060,
        "name": "Vivek Ravi",
        "username": "r_vivek",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/827d123f41e8af1a8c14b147564b908b?s=80&d=identicon",
        "web_url": "https://gitlab.com/r_vivek"
    },
    "started_at": null,
    "finished_at": null,
    "committed_at": null,
    "duration": null,
    "coverage": null,
    "detailed_status": {
        "icon": "status_pending",
        "text": "pending",
        "label": "pending",
        "group": "pending",
        "tooltip": "pending",
        "has_details": true,
        "details_path": "/r_vivek/Petstore/-/pipelines/233052784",
        "illustration": null,
        "favicon": "https://gitlab.com/assets/ci_favicons/favicon_status_pending-5bdf338420e5221ca24353b6bff1c9367189588750632e9a871b7af09ff6a2ae.png"
    }
}
```
- The _web_url_ in the response will navigate to the pipeline triggered by the POST call
- After the jobs are completed, the report artifacts can be viewed by following the below steps

Click on the test job after navigating to the pipeline
![](src/docs/pipeline-job.png)

Click on the Browse option under artifacts section
![](src/docs/artifact-browse.png)

Navigate all the way to Artifacts/target/site/serenity
![](src/docs/navigate.png)

Click on the symbol near index.html to open the report in the same browser
![](src/docs/index.png)

## Important Note

The test job will fail since some of the scenarios in Petsore swagger API is not working as intended. It is probably some bugs on the end point
For eg. When username/password is incorrect in the login service, the test should return 400 response but it is currently returning 200
