package starter.steps;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.commons.collections.ListUtils;
import starter.WebServiceEndPoints;
import starter.models.Pet;
import starter.utils.ReuseableSpecifications;
import starter.utils.TestUtils;

import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class PetSteps {

    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String PETENDPOINT = WebServiceEndPoints.PETENDPOINT.getUrl();
    private static String FINDBYSTATUSENDPOINT = WebServiceEndPoints.FINDBYSTATUSENDPOINT.getUrl();
    private static String FINDBYTAGSENDPOINT = WebServiceEndPoints.FINDBYTAGSENDPOINT.getUrl();


    @Step("Post the test data {0} at the pet end point")
    public void createPet(String fileName) {
        Pet pet = (Pet) TestUtils.toObject(fileName, Pet.class);
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(pet)
                .post(BASEPATH + PETENDPOINT);
    }

    @Step("check whether the pet response is same as the test data {0}")
    public void validateResponse(String fileName) {
        Pet pet = (Pet) TestUtils.toObject(fileName, Pet.class);
        assertThat(SerenityRest.and()
                .assertThat()
                .extract()
                .body()
                .as(Pet.class)).isEqualTo(pet);
    }

    @Step("Update the test data from {0} at the pet end point")
    public void updatePet(String fileName) {
        Pet pet = (Pet) TestUtils.toObject(fileName, Pet.class);
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(pet)
                .put(BASEPATH + PETENDPOINT);
    }


    @Step("Get the test data mentioned in {0} from the pet end point")
    public void getPet(String fileName) {
        Pet pet = (Pet) TestUtils.toObject(fileName, Pet.class);
        SerenityRest.given()
                .pathParam("id", pet.getId())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .get(BASEPATH + PETENDPOINT + "/{id}");
    }

    @Step("Delete the test data {0} from the pet end point")
    public void deletePet(String fileName) {
        Pet pet = (Pet) TestUtils.toObject(fileName, Pet.class);
        SerenityRest.given()
                .pathParam("id", pet.getId())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .delete(BASEPATH + PETENDPOINT + "/{id}");
    }

    @Step("Find all pets where status : {0}")
    public void findByStatus(String status) {
        SerenityRest.given()
                .queryParam("status", status)
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get(BASEPATH + FINDBYSTATUSENDPOINT);
    }

    @Step("Find all pets with tags : {0}")
    public void findByTags(List<String> tags) {
        SerenityRest.given()
                .queryParam("tags", tags)
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get(BASEPATH + FINDBYTAGSENDPOINT);
    }

    @Step("Create a random pet with status{0}")
    public void createPetWithStatus(String status) {
        Pet pet = new Pet(new Random().nextInt(Integer.MAX_VALUE), null, "random", ListUtils.EMPTY_LIST, ListUtils.EMPTY_LIST, status);
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(pet)
                .put(BASEPATH + PETENDPOINT);
    }

    @Step("Get the pet with id : {0}")
    public void gePetById(int id) {
        SerenityRest.given()
                .pathParam("id", id)
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .get(BASEPATH + PETENDPOINT + "/{id}");
    }
}
