package starter.steps;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import starter.WebServiceEndPoints;
import starter.utils.ReuseableSpecifications;

public class CommonSteps {

    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String ENDPOINTCHECK = WebServiceEndPoints.ENDPOINTCHECK.getUrl();

    @Step("Access the base end point")
    public void accessBaseEndPoint() {
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get(BASEPATH + ENDPOINTCHECK);
    }

    @Step("Check whether the response is {0}")
    public void checkResponse(int response) {
        SerenityRest.then()
                .spec(ReuseableSpecifications.getGenericResponseSpec())
                .statusCode(response);
    }


}
