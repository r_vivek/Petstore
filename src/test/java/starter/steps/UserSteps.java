package starter.steps;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import starter.WebServiceEndPoints;
import starter.models.ApiResponse;
import starter.models.User;
import starter.utils.ReuseableSpecifications;
import starter.utils.TestUtils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UserSteps {

    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String USERENDPOINT = WebServiceEndPoints.USERENDPOINT.getUrl();
    private static String USERLOGIN = WebServiceEndPoints.USERLOGIN.getUrl();
    private static String USERLOGOUT = WebServiceEndPoints.USERLOGOUT.getUrl();
    private static String CREATEUSERLIST = WebServiceEndPoints.CREATEUSERLIST.getUrl();


    @Step("Post the test data {0} at the user end point")
    public void createUser(String fileName) {
        User user = (User) TestUtils.toObject(fileName, User.class);
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(user)
                .post(BASEPATH + USERENDPOINT);
    }

    @Step("check whether the user response is same as the test data {0}")
    public void validateResponse(String fileName) {
        User user = (User) TestUtils.toObject(fileName, User.class);
        assertThat(SerenityRest.and()
                .assertThat()
                .extract()
                .body()
                .as(User.class)).isEqualTo(user);
    }

    @Step("Update the user data from {0} at the user end point")
    public void updateUser(String fileName) {
        User user = (User) TestUtils.toObject(fileName, User.class);
        SerenityRest.given()
                .pathParam("username", user.getUsername())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(user)
                .put(BASEPATH + USERENDPOINT + "/{username}");
    }


    @Step("Get the test data mentioned in {0} from the user end point")
    public void getUser(String fileName) {
        User user = (User) TestUtils.toObject(fileName, User.class);
        SerenityRest.given()
                .pathParam("username", user.getUsername())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .get(BASEPATH + USERENDPOINT + "/{username}");
    }

    @Step("Get the user whose userName is {0} from the user end point")
    public void getUserByUserName(String userName) {
        SerenityRest.given()
                .pathParam("username", userName)
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .get(BASEPATH + USERENDPOINT + "/{username}");
    }

    @Step("Delete the test data {0} from the user end point")
    public void deleteUser(String fileName) {
        User user = (User) TestUtils.toObject(fileName, User.class);
        SerenityRest.given()
                .pathParam("username", user.getUsername())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .delete(BASEPATH + USERENDPOINT + "/{username}");
    }

    @Step("Check whether the userID in the API response is the same userID as in the test data {0}")
    public void validateID(String fileName) {
        User user = (User) TestUtils.toObject(fileName, User.class);
        ApiResponse apiResponse = SerenityRest.and()
                .extract()
                .body()
                .as(ApiResponse.class);
        assertThat(apiResponse.getMessage()).isEqualTo(user.getId().toString());
    }

    @Step("Login to petstore with userName : {0} and password : {1}")
    public void login(String userName, String password) {
        SerenityRest.given()
                .queryParam("username", userName)
                .queryParam("password", password)
                .spec(ReuseableSpecifications.getLoginRequestSpecification())
                .get(BASEPATH + USERLOGIN);
    }

    @Step("Logout from the petstore using the logout end point")
    public void logout() {
        SerenityRest.given()
                .spec(ReuseableSpecifications.getLoginRequestSpecification())
                .get(BASEPATH + USERLOGOUT);
    }

    @Step("Add the list of users in the list {0}")
    public void addMultipleUsers(List<User> users) {
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(users)
                .post(BASEPATH + CREATEUSERLIST);
    }
}
