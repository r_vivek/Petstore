package starter.steps;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import starter.WebServiceEndPoints;
import starter.models.Order;
import starter.utils.ReuseableSpecifications;
import starter.utils.TestUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderSteps {

    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String ORDERENDPOINT = WebServiceEndPoints.ORDERENDPOINT.getUrl();
    private static String INVENTORYENDPOINT = WebServiceEndPoints.INVENTORYENDPOINT.getUrl();

    @Step("Post the test data {0} at the order end point")
    public void postOrder(String fileName) {
        Order order = (Order) TestUtils.toObject(fileName, Order.class);
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(order)
                .post(BASEPATH + ORDERENDPOINT);
    }

    @Step("check whether the order response is same as the test data {0}")
    public void validateResponse(String fileName) {
        Order order = (Order) TestUtils.toObject(fileName, Order.class);
        //API sends extra 0s so manual override so that tests are passing
        order.setShipDate(order.getShipDate().concat("+0000"));
        assertThat(SerenityRest.and()
                .assertThat()
                .extract()
                .body()
                .as(Order.class)).isEqualTo(order);
    }

    @Step("Post the test data from {0} at the order end point")
    public void updateOrder(String fileName) {
        Order order = (Order) TestUtils.toObject(fileName, Order.class);
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(order)
                .put(BASEPATH + ORDERENDPOINT);
    }

    @Step("Get the test data mentioned in {0} from the order end point")
    public void getOrder(String fileName) {
        Order order = (Order) TestUtils.toObject(fileName, Order.class);
        SerenityRest.given()
                .pathParam("id", order.getId())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .get(BASEPATH + ORDERENDPOINT + "/{id}");
    }

    @Step("Delete the test data {0} from the order end point")
    public void deleteOrder(String fileName) {
        Order order = (Order) TestUtils.toObject(fileName, Order.class);
        SerenityRest.given()
                .pathParam("id", order.getId())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .delete(BASEPATH + ORDERENDPOINT + "/{id}");
    }

    @Step("Call the order inventory service")
    public void orderInventory() {
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .get(BASEPATH + INVENTORYENDPOINT);
    }

    @Step("Get the quantity of pets whose status : {0}")
    public int getQuantityOfPetsWhoseStatus(String status) {
        orderInventory();
        return SerenityRest
                .then()
                .extract()
                .response()
                .body()
                .path(status) == null ? 0 : SerenityRest
                .then()
                .extract()
                .response()
                .body()
                .path(status);
    }

    @Step("Get the order with id : {0} from the order end point")
    public void getOrderById(int id) {
        SerenityRest.given()
                .pathParam("id", id)
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .get(BASEPATH + ORDERENDPOINT + "/{id}");
    }
}
