package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.steps.CommonSteps;

public class CommonStepDefinitions {


    private static int HTTPS_OK = 200;

    @Steps
    CommonSteps commonSteps;

    @When("I access the pet store endpoint")
    public void iAccessThePetStoreEndpoint() {
        commonSteps.accessBaseEndPoint();
    }

    @Then("the API should return a {int} response")
    public void theAPIShouldReturnAResponse(int response) {
        commonSteps.checkResponse(response);
    }

    @Given("The endpoint is accessible")
    public void theEndpointIsAccessible() {
        commonSteps.accessBaseEndPoint();
        commonSteps.checkResponse(HTTPS_OK);
    }

    @Then("the API has to return a {int} response")
    public void theAPIHasToReturnAResponse(int status) {
        SerenityRest.then()
                .statusCode(status);
    }

}
