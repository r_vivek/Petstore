package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.apache.commons.lang3.EnumUtils;
import starter.WebServiceEndPoints;
import starter.models.Pet;
import starter.models.PetStatus;
import starter.steps.CommonSteps;
import starter.steps.PetSteps;
import starter.utils.ReuseableSpecifications;

import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItems;

public class PetStepDefinitions {

    private static int HTTPS_OK = 200;
    private static int HTTPS_NOK = 404;
    private static int randomId = 0;
    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String PETENDPOINT = WebServiceEndPoints.PETENDPOINT.getUrl();

    @Steps
    CommonSteps commonSteps;

    @Steps
    PetSteps petSteps;

    @When("I post the pet from {string}")
    public void iPostThePetFrom(String fileName) {
        petSteps.createPet(fileName);
    }

    @And("pet in the response body should have same details as {string}")
    public void response_body_should_have_same_details_as(String fileName) {
        petSteps.validateResponse(fileName);
    }


    @When("I update the pet with the details from the testdata : {string}")
    public void iUpdateThePetWithTheDetailsFromTheTestdata(String fileName) {
        petSteps.updatePet(fileName);
    }

    @When("I get the pet with the details from the testdata : {string}")
    public void iGetThePetWithTheDetailsFromTheTestdata(String fileName) {
        petSteps.getPet(fileName);
    }

    @Given("The pet in the test data {string} exists")
    public void thePetInTheTestDataExists(String fileName) {
        petSteps.getPet(fileName);
        commonSteps.checkResponse(HTTPS_OK);
    }

    @Given("The pet is updated successfully")
    public void thePetIsUpdated() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @When("I delete the pet with the details from the testdata : {string} from the end point")
    public void iDeleteThePetWithTheDetailsFromTheTestdataFromTheEndPoint(String fileName) {
        petSteps.deletePet(fileName);
    }

    @Given("The pet is deleted successfully")
    public void thePetIsDeletedSuccessfully() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @When("I call the pet findByStatus end point with status : {string}")
    public void iCallThePetFindByStatusEndPointWithStatus(String status) {
        assertThat(EnumUtils.isValidEnum(PetStatus.class, status));
        petSteps.findByStatus(status);
    }

    @Then("the API response should have only pets with status : {string}")
    public void theAPIResponseShouldHaveOnlyPetsWithStatus(String status) {
        assertThat(EnumUtils.isValidEnum(PetStatus.class, status));
        SerenityRest.then()
                .assertThat()
                .body("status", everyItem(equalTo(status)));
    }

    @When("I call the pet findByTags end point with tags :")
    public void iCallThePetFindByTagsEndPointWithTags(List<String> tags) {
        petSteps.findByTags(tags);
    }

    @Then("the API response should have only pets whose tags are")
    public void theAPIResponseShouldHaveOnlyPetsWhoseTagsAre(List<String> tags) {
        SerenityRest.then()
                .assertThat()
                .body("tags.name", hasItems(tags));
    }

    @Given("The pet with a random ID does not exist")
    public void the_pet_with_a_random_ID_does_not_exist() {
        randomId = new Random().nextInt(Integer.MAX_VALUE);
        petSteps.gePetById(randomId);
        commonSteps.checkResponse(HTTPS_NOK);
    }

    @When("I update the pet with the random ID")
    public void i_update_the_pet_with_the_random_ID() {
        Pet pet = new Pet();
        pet.setId(randomId);
        SerenityRest.given()
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(pet)
                .put(BASEPATH + PETENDPOINT);
    }

    @When("I delete the pet with the random ID")
    public void iDeleteThePetWithTheRandomID() {
        SerenityRest.given()
                .pathParam("id", randomId)
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .delete(BASEPATH + PETENDPOINT + "/{id}");
    }

}