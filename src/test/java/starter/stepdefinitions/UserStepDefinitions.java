package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import starter.WebServiceEndPoints;
import starter.models.User;
import starter.steps.CommonSteps;
import starter.steps.UserSteps;
import starter.utils.ReuseableSpecifications;
import starter.utils.TestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserStepDefinitions {

    private static int HTTPS_OK = 200;
    private static int HTTPS_NOK = 404;
    private static String randomUser = "";
    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String USERENDPOINT = WebServiceEndPoints.USERENDPOINT.getUrl();

    @Steps
    CommonSteps commonSteps;

    @Steps
    UserSteps userSteps;

    @When("I post the user from {string}")
    public void iPostTheUserFrom(String fileName) {
        userSteps.createUser(fileName);
    }

    @And("user in the response body should have same details as {string}")
    public void userInTheResponseBodyShouldHaveSameDetailsAs(String fileName) {
        userSteps.validateResponse(fileName);
    }

    @Given("The user in the test data {string} exists")
    public void theUserInTheTestDataExists(String fileName) {
        userSteps.getUser(fileName);
        commonSteps.checkResponse(HTTPS_OK);
    }

    @When("I update the user with the details from the testdata : {string}")
    public void iUpdateTheUserWithTheDetailsFromTheTestdata(String fileName) {
        userSteps.updateUser(fileName);
    }

    @Given("The user is updated successfully")
    public void theUserIsUpdatedSuccessfully() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @When("I get the user with the details from the testdata : {string}")
    public void iGetTheUserWithTheDetailsFromTheTestdata(String fileName) {
        userSteps.getUser(fileName);
    }

    @When("I delete the user with the details from the testdata : {string} from the end point")
    public void iDeleteTheUserWithTheDetailsFromTheTestdataFromTheEndPoint(String fileName) {
        userSteps.deleteUser(fileName);
    }

    @Given("The user is deleted successfully")
    public void theUserIsDeletedSuccessfully() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @And("userID in the response body should have same userID as in {string}")
    public void useridInTheResponseBodyShouldHaveSameUserIDAsIn(String fileName) {
        userSteps.validateID(fileName);
    }

    @Given("The user with username : {string} exists")
    public void theUserWithUsernameExists(String userName) {
        userSteps.getUserByUserName(userName);
        commonSteps.checkResponse(HTTPS_OK);
    }

    @When("I login with username : {string} and password : {string}")
    public void iLoginWithUsernameAndPassword(String userName, String password) {
        userSteps.login(userName, password);
    }

    @Given("The user is logged in")
    public void theUserIsLoggedIn() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @When("I logout from the petstore app using the logout end point")
    public void iLogoutFromThePetstoreAppUsingTheLogoutEndPoint() {
        userSteps.logout();
    }

    @When("I call the createWithList end point with the below list of users :")
    public void iCallTheCreateWithListEndPointWithTheBelowListOfUsers(DataTable dataTable) {
        List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        List<User> users = new ArrayList<>();
        rows.forEach(row ->
                users.add((User) TestUtils.toObject(row, User.class)));
        userSteps.addMultipleUsers(users);
    }

    @Given("The user with a random userName does not exist")
    public void theUserWithARandomUserNameDoesNotExist() {
        randomUser=TestUtils.generateRandomString(8);
        userSteps.getUserByUserName(randomUser);
        commonSteps.checkResponse(HTTPS_NOK);
    }

    @When("I update the user with the random userName")
    public void iUpdateTheUserWithTheRandomUserName() {
        User user = new User();
        user.setUsername(randomUser);
        SerenityRest.given()
                .pathParam("username", user.getUsername())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .body(user)
                .put(BASEPATH + USERENDPOINT + "/{username}");
    }

    @When("I delete the user with the random userName")
    public void iDeleteTheUserWithTheRandomUserName() {
        randomUser=TestUtils.generateRandomString(8);
        User user = new User();
        user.setUsername(randomUser);
        SerenityRest.given()
                .pathParam("username", user.getUsername())
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .delete(BASEPATH + USERENDPOINT + "/{username}");
    }

    @When("I login with random userName which does not exist")
    public void iLoginWithRandomUserNameWhichDoesNotExist() {
        userSteps.login(randomUser, randomUser);
    }
}
