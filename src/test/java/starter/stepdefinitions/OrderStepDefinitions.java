package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.WebServiceEndPoints;
import starter.steps.CommonSteps;
import starter.steps.OrderSteps;
import starter.steps.PetSteps;
import starter.utils.ReuseableSpecifications;

import java.util.Random;

import static org.hamcrest.Matchers.equalTo;

public class OrderStepDefinitions {

    private static int HTTPS_OK = 200;
    private static int HTTPS_NOK = 404;

    @Steps
    CommonSteps commonSteps;

    @Steps
    OrderSteps orderSteps;

    @Steps
    PetSteps petSteps;

    private static int quantity = 0;
    private static int randomId = 0;
    private static String BASEPATH = WebServiceEndPoints.BASEPATH.getUrl();
    private static String ORDERENDPOINT = WebServiceEndPoints.ORDERENDPOINT.getUrl();


    @When("I post the order from {string}")
    public void iPostTheOrderFrom(String fileName) {
        orderSteps.postOrder(fileName);
    }

    @And("order in the response body should have same details as {string}")
    public void responseBodyShouldHaveSameDetailsAs(String fileName) {
        orderSteps.validateResponse(fileName);
    }

    @Given("The order in the test data {string} exists")
    public void theOrderInTheTestDataExists(String fileName) {
        orderSteps.getOrder(fileName);
        commonSteps.checkResponse(HTTPS_OK);
    }

    @When("I update the order with the details from the testdata : {string}")
    public void iUpdateTheOrderWithTheDetailsFromTheTestdata(String fileName) {
        orderSteps.updateOrder(fileName);
    }

    @Given("The order is added successfully")
    public void theOrderIsUpdatedSuccessfully() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @When("I get the order with the details from the testdata : {string}")
    public void iGetTheOrderWithTheDetailsFromTheTestdata(String fileName) {
        orderSteps.getOrder(fileName);
    }

    @When("I delete the order with the details from the testdata : {string} from the end point")
    public void iDeleteTheOrderWithTheDetailsFromTheTestdataFromTheEndPoint(String fileName) {
        orderSteps.deleteOrder(fileName);
    }

    @Given("The order is deleted successfully")
    public void theOrderIsDeletedSuccessfully() {
        SerenityRest.lastResponse().then().statusCode(HTTPS_OK);
    }

    @Given("Both the endpoints are accessible")
    public void bothTheEndpointsAreAccessible() {
        commonSteps.accessBaseEndPoint();
        commonSteps.checkResponse(HTTPS_OK);
        orderSteps.orderInventory();
        SerenityRest.then().statusCode(HTTPS_OK);
    }

    @And("quantity of pets with status : {string} is noted")
    public void quantityOfPetsWithStatusIsNoted(String status) {
        quantity = orderSteps.getQuantityOfPetsWhoseStatus(status);
    }

    @And("{int} pets are created in the pet store via the pet end point with status : {string}")
    public void petsAreCreatedInThePetStoreViaThePetEndPointWithStatus(int quantity, String status) {
        for (int i = 0; i < quantity; i++) {
            petSteps.createPetWithStatus(status);
            commonSteps.checkResponse(HTTPS_OK);
        }
    }

    @When("I call the order inventory end point")
    public void iCallTheOrderInventoryEndPoint() {
        orderSteps.orderInventory();
    }

    @Then("the API response should have the quantity of pets with status : {string} increased by {int}")
    public void theAPIResponseShouldHaveTheQuantityOfPetsWithStatusIncreasedBy(String status, int increment) {
        SerenityRest.then()
                .assertThat()
                .body(status, equalTo(quantity + increment));
    }

    @Given("The order with a random ID does not exist")
    public void theOrderWithARandomIDDoesNotExist() {
        randomId = new Random().nextInt(Integer.MAX_VALUE);
        orderSteps.getOrderById(randomId);
        commonSteps.checkResponse(HTTPS_NOK);
    }

    @When("I delete the order with the random ID")
    public void iDeleteTheOrderWithTheRandomID() {
        SerenityRest.given()
                .pathParam("id", randomId)
                .spec(ReuseableSpecifications.getGenericRequestSpec())
                .when()
                .delete(BASEPATH + ORDERENDPOINT + "/{id}");
    }


}