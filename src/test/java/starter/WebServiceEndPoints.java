package starter;

public enum WebServiceEndPoints {
    BASEPATH("https://petstore.swagger.io/v2"),
    ENDPOINTCHECK("/swagger.json"),
    PETENDPOINT("/pet"),
    FINDBYSTATUSENDPOINT("/pet/findByStatus"),
    FINDBYTAGSENDPOINT("/pet/findByTags"),
    ORDERENDPOINT("/store/order"),
    INVENTORYENDPOINT("/store/inventory"),
    USERENDPOINT("/user"),
    USERLOGIN("/user/login"),
    USERLOGOUT("/user/logout"),
    CREATEUSERLIST("/user/createWithList");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
