package starter.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Map;

public class TestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestUtils.class);

    public static Object toObject(final String fileName, final Class aClass) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            File file = new ClassPathResource("testfiles/" + fileName).getFile();
            return mapper.readValue(file, aClass);
        } catch (IOException e) {
            LOGGER.error("Exception while reading the JSON file : {}", e.getMessage());
        }
        return null;
    }

    public static Object toObject(final Map map, final Class aClass) {
        ObjectMapper mapper = new ObjectMapper();
        final Object o = mapper.convertValue(map, aClass);
        return o;
    }

    public static String generateRandomString(int length) {
        // You can customize the characters that you want to add into
        // the random strings
        String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        String CHAR_UPPER = CHAR_LOWER.toUpperCase();
        String NUMBER = "0123456789";

        String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
        SecureRandom random = new SecureRandom();

        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

            sb.append(rndChar);
        }

        return sb.toString();
    }
}
