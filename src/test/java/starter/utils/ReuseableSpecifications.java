package starter.utils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class ReuseableSpecifications {

    public static RequestSpecBuilder rspec;
    public static RequestSpecification requestSpecification;

    public static ResponseSpecBuilder respec;
    public static ResponseSpecification responseSpecification;


    public static RequestSpecification getGenericRequestSpec() {
        rspec = new RequestSpecBuilder();
        rspec.setContentType(ContentType.JSON);
        rspec.setAccept(ContentType.JSON);
        rspec.setRelaxedHTTPSValidation();
        requestSpecification = rspec.build();
        return requestSpecification;
    }

    public static ResponseSpecification getGenericResponseSpec() {
        respec = new ResponseSpecBuilder();
        respec.expectHeader("Content-Type", "application/json");
        respec.expectHeader("Transfer-Encoding", "chunked");
        responseSpecification = respec.build();
        return responseSpecification;

    }

    public static RequestSpecification getLoginRequestSpecification() {
        rspec = new RequestSpecBuilder();
        rspec.setContentType(ContentType.JSON);
        rspec.setAccept(ContentType.JSON);
        rspec.setRelaxedHTTPSValidation();
        rspec.addHeader("X-Expires-After","Wed, 14 Jun 2020 07:00:00 GMT");
        rspec.addHeader("X-Rate-Limit","100");
        requestSpecification = rspec.build();
        return requestSpecification;
    }
}
