package starter.models;

public enum OrderStatus {
    placed, approved, delivered;
}
