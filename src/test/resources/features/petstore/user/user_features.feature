Feature: User features in the pet store

  @SmokeTest @E2ETest
  Scenario: Check availability of petstore app
    When I access the pet store endpoint
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Adding a user to the petstore app
    Given The endpoint is accessible
    When I post the user from "user.json"
    Then the API should return a 200 response
    And userID in the response body should have same userID as in "user.json"

  @SmokeTest @E2ETest
  Scenario: Update a user from the petstore app and verify the response
    Given The user in the test data "user.json" exists
    When I update the user with the details from the testdata : "user_updated.json"
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Get the updated user from the petstore app and verify the response
    Given The user is updated successfully
    When I get the user with the details from the testdata : "user_updated.json"
    Then the API should return a 200 response
    And user in the response body should have same details as "user_updated.json"

  @SmokeTest @E2ETest
  Scenario: Check whether the user is able to login to the petstore app using the login end point
    Given The user with username : "Test_UserName_updated" exists
    When I login with username : "Test_UserName_updated" and password : "gmail123_updated"
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Check whether the user is able to logout to the petstore app using the logout end point
    Given The user is logged in
    When I logout from the petstore app using the logout end point
    Then the API should return a 200 response

  @NegativeTest @E2ETest
  Scenario: Check whether the user is getting a 400 response when trying to login with wrong password
    Given The user with username : "Test_UserName_updated" exists
    When I login with username : "Test_UserName_updated" and password : "wrong_password"
    Then the API should return a 400 response

  @SmokeTest @E2ETest
  Scenario: Delete the updated user from the petstore app and verify the response
    Given The user in the test data "user_updated.json" exists
    When I delete the user with the details from the testdata : "user_updated.json" from the end point
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Check whether the updated user has been deleted from the petstore app
    Given The user is deleted successfully
    When I get the user with the details from the testdata : "user_updated.json"
    Then the API should return a 404 response


  @SmokeTest @E2ETest
  Scenario: Add a list of users to the pets store using the createWithList end point
    Given The endpoint is accessible
    When I call the createWithList end point with the below list of users :
      | id  | username     | firstName     | lastName     | email         | password     | phone | userStatus |
      | 111 | username_111 | firstname_111 | lastname_111 | 111@gmail.com | 111_password | 111   | 0          |
      | 222 | username_222 | firstname_222 | lastname_222 | 222@gmail.com | 222_password | 222   | 0          |
      | 333 | username_333 | firstname_333 | lastname_333 | 333@gmail.com | 333_password | 333   | 0          |
    Then the API should return a 200 response


  @NegativeTest @E2ETest
  Scenario: When updating a user from the petstore app with a non-existent userName
    Given The user with a random userName does not exist
    When I update the user with the random userName
    Then the API should return a 404 response

  @NegativeTest @E2ETest
  Scenario: When deleting a user from the petstore app with a non-existent userName
    Given The user with a random userName does not exist
    When I delete the user with the random userName
    Then the API has to return a 404 response

  @NegativeTest @E2ETest
  Scenario: Check whether the user is getting a 400 response when trying to login with invalid username
    Given The user with a random userName does not exist
    When I login with random userName which does not exist
    Then the API should return a 400 response