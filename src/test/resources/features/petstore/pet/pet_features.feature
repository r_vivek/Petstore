Feature: Pet features in the pet store

  @SmokeTest @E2ETest
  Scenario: Check availability of petstore app
    When I access the pet store endpoint
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Adding a pet to the petstore app
    Given The endpoint is accessible
    When I post the pet from "buffy.json"
    Then the API should return a 200 response
    And pet in the response body should have same details as "buffy.json"

  @SmokeTest @E2ETest
  Scenario: Update a pet from the petstore app and verify the response
    Given The pet in the test data "buffy.json" exists
    When I update the pet with the details from the testdata : "buffy_updated.json"
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Get the updated pet from the petstore app and verify the response
    Given The pet is updated successfully
    When I get the pet with the details from the testdata : "buffy_updated.json"
    Then the API should return a 200 response
    And pet in the response body should have same details as "buffy_updated.json"

  @SmokeTest @E2ETest
  Scenario: Get all the pending pets from the petstore app and verify in the response whether all pets are sold
    Given The endpoint is accessible
    When I call the pet findByStatus end point with status : "pending"
    Then the API response should have only pets with status : "pending"

  @SmokeTest @E2ETest
  Scenario: Get all the pets with tags from the petstore app and verify in the response whether all pets have the same tags
    Given The endpoint is accessible
    When I call the pet findByTags end point with tags :
      | rottweiler_updated |
      | friendly_updated |
    Then the API response should have only pets whose tags are
      | rottweiler_updated |
      | friendly_updated |

  @SmokeTest @E2ETest
  Scenario: Delete the updated pet from the petstore app and verify the response
    Given The pet in the test data "buffy_updated.json" exists
    When I delete the pet with the details from the testdata : "buffy_updated.json" from the end point
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Check whether the updated pet has been deleted from the petstore app
    Given The pet is deleted successfully
    When I get the pet with the details from the testdata : "buffy_updated.json"
    Then the API should return a 404 response

  @NegativeTest @E2ETest
  Scenario: When updating a pet from the petstore app with a non-existent ID
    Given The pet with a random ID does not exist
    When I update the pet with the random ID
    Then the API should return a 404 response

  @NegativeTest @E2ETest
  Scenario: When deleting a pet from the petstore app with a non-existent ID
    Given The pet with a random ID does not exist
    When I delete the pet with the random ID
    Then the API has to return a 404 response

  @NegativeTest @E2ETest
  Scenario: When calling the findByStatus service with an invalid status
    Given The endpoint is accessible
    When I call the pet findByStatus end point with status : "not_valid"
    Then the API should return a 400 response