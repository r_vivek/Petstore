Feature: Order features in the pet store

  @SmokeTest @E2ETest
  Scenario: Check availability of petstore app
    When I access the pet store endpoint
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Adding an order to the petstore app
    Given The endpoint is accessible
    When I post the order from "order.json"
    Then the API should return a 200 response
    And order in the response body should have same details as "order.json"

  @SmokeTest @E2ETest
  Scenario: Get the order from the petstore app and verify the response
    Given The order is added successfully
    When I get the order with the details from the testdata : "order.json"
    Then the API should return a 200 response
    And order in the response body should have same details as "order.json"

  @SmokeTest @E2ETest
  Scenario: Delete the order from the petstore app and verify the response
    Given The order in the test data "order.json" exists
    When I delete the order with the details from the testdata : "order.json" from the end point
    Then the API should return a 200 response

  @SmokeTest @E2ETest
  Scenario: Check whether the updated order has been deleted from the petstore app
    Given The order is deleted successfully
    When I get the order with the details from the testdata : "order.json"
    Then the API should return a 404 response

  @SmokeTest @E2ETest
  Scenario: Create new pets in the petstore via the pet end point and call the order inventory service to verify the response
    Given Both the endpoints are accessible
    And quantity of pets with status : "sold" is noted
    And 5 pets are created in the pet store via the pet end point with status : "sold"
    When I call the order inventory end point
    Then the API response should have the quantity of pets with status : "sold" increased by 5

  @NegativeTest @E2ETest
  Scenario: Update an order from the petstore app and verify whether 405 response is received
    When I update the order with the details from the testdata : "order.json"
    Then the API should return a 405 response

  @NegativeTest @E2ETest
  Scenario: When deleting an order from the petstore app with a non-existent ID
    Given The order with a random ID does not exist
    When I delete the order with the random ID
    Then the API has to return a 404 response